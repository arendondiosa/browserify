(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var operations = require('./operations');
var msj = require('./msj');
var a = 10;
var b = 7;

var div = document.getElementById('test');
div.style.color = "rgba(8, 67, 136, 0.79)";

var p1 = document.createElement("p");
var p2 = document.createElement("p");
var p3 = document.createElement("p");
var p4 = document.createElement("p");
var p5 = document.createElement("p");

p1.innerText = `${a} + ${b} = ${operations.add(a, b)}`;
p2.innerText = `${a} - ${b} = ${operations.sub(a, b)}`;
p3.innerText = `${a} * ${b} = ${operations.mult(a, b)}`;
p4.innerText = `${a} / ${b} = ${operations.div(a, b)} Resiiduo: ${operations.mod(a, b)}`;
p5.innerText = `${msj.text}`;
p5.style.color = "rgba(189, 36, 15, 0.86)";

div.appendChild(p1);
div.appendChild(p2);
div.appendChild(p3);
div.appendChild(p4);
div.appendChild(p5);

},{"./msj":2,"./operations":3}],2:[function(require,module,exports){
module.exports = {
  text: "Que coooool!!!"
}

},{}],3:[function(require,module,exports){
module.exports = {
  add: function(a, b) {
    return a + b;
  },
  sub: function(a, b) {
    return a - b;
  },
  mult: function(a, b) {
    return a * b;
  },
  div: function(a, b) {
    return Math.floor(a / b);
  },
  mod: function(a, b) {
    return a % b;
  }
}

},{}]},{},[1]);
