var operations = require('./operations');
var msj = require('./msj');
var a = 10;
var b = 7;

var div = document.getElementById('test');
div.style.color = "rgba(8, 67, 136, 0.79)";

var p1 = document.createElement("p");
var p2 = document.createElement("p");
var p3 = document.createElement("p");
var p4 = document.createElement("p");
var p5 = document.createElement("p");

p1.innerText = `${a} + ${b} = ${operations.add(a, b)}`;
p2.innerText = `${a} - ${b} = ${operations.sub(a, b)}`;
p3.innerText = `${a} * ${b} = ${operations.mult(a, b)}`;
p4.innerText = `${a} / ${b} = ${operations.div(a, b)} Resiiduo: ${operations.mod(a, b)}`;
p5.innerText = `${msj.text}`;
p5.style.color = "rgba(189, 36, 15, 0.86)";

div.appendChild(p1);
div.appendChild(p2);
div.appendChild(p3);
div.appendChild(p4);
div.appendChild(p5);
