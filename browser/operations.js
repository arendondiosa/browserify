module.exports = {
  add: function(a, b) {
    return a + b;
  },
  sub: function(a, b) {
    return a - b;
  },
  mult: function(a, b) {
    return a * b;
  },
  div: function(a, b) {
    return Math.floor(a / b);
  },
  mod: function(a, b) {
    return a % b;
  }
}
